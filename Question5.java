package java8;
import java.util.Scanner;

public class Question5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		System.out.println("Enter employee name:");
		String name=s.next();
		System.out.println("Enter employee id");
		String empid=s.next();
		System.out.println("Enter basic salary:");
		int basic=s.nextInt();
		System.out.println("Enter special allowances:");
		int specall=s.nextInt();
		System.out.println("Enter percentage of bonus:");
		double percent=s.nextDouble();
		int monthlygross=basic+specall;
		int annualsalary=monthlygross*12;
		double bonusamount=annualsalary*percent/100;
		double gas=annualsalary+bonusamount;
		if(gas<250000)
		{
			System.out.println("Annual Net Salary is:"+gas);
		}
		else if(gas>=250000 && gas<500000)
		{
			gas=gas*95/100;
			System.out.println("Annual Net Salary is:"+gas);
		}
		else if(gas>=500000 && gas<100000)
		{
			gas=gas*80/100;
			System.out.println("Annual Net Salary is:"+gas);
		}
		else
		{
			gas=gas*70/100;
			System.out.println("Annual Net Salary is:"+gas);
		}
		

	}

}
