package java8;
import java.util.Scanner;
import java.util.Arrays;
class search
{
	public int binarysearch(int a[],int n,int key)
	{
		int ans=-1;
		int i=0;
		int j=n;
		while(i<j)
		{
			int mid=(i+j)/2;
			if(a[mid]==key)
			{
				ans=mid;
				break;
			}
			else if (a[mid]>key)
			{
				j=mid=1;
			}
			else
			{
				i=mid+1;
			}
		}
		return ans;
	}
}
public class Question18 {
	public static void main(String args[])
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the size of the array:");
		int n=s.nextInt();
		int a[]=new int[n];
		System.out.println("Enter the array elements:");
		for(int i=0;i<n;i++)
		{
			a[i]=s.nextInt();
		}
		System.out.println("Elements present in the array are:");
		for(int j=0;j<n;j++)
		{
			System.out.print(a[j]+" ");
		}
		Arrays.sort(a);
		System.out.println();
		System.out.println("Enter the element to be searched:");
		int key=s.nextInt();
		search s1=new search();
		int flag=s1.binarysearch(a, n, key);
		if(flag==-1)
		{
			System.out.println("Element not found!");
		}
		else
		{
			System.out.println("Element found at index "+flag);
		}
		
		
	}

}
