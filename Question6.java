package java8;
import java.util.Scanner;
public class Question6 {
	public static void main(String args[])
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Enter an integer n:");
		int n=s.nextInt();
		for(int i=1;i<=n;i++)
		{
			int temp=i*i;
			temp=temp-(i-1);
			System.out.print(temp+" ");
		}
		s.close();
	}

}
