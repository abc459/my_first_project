package java8;
import java.util.Scanner;

public class Question19 {
	public static void main(String args[])
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Enter n:");
		int n=s.nextInt();
		int a[][]=new int[n][n];
		System.out.println("Enter the elements of matrix:");
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				a[i][j]=s.nextInt();
			}
		}
		int flag=0;
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				if(a[i][j]!=0)
				{
					if(a[i][j]==1 && i==j)
					{
						continue;
					}
					else
					{
						flag=1;
						break;
					
					}
				}
			}
			if(flag==1)
			{
				break;
			}
		}
		if(flag==1)
		{
			System.out.println("Not an identity martix!");
			
		}
		else
		{
			System.out.println("Indentity matrix!");
		}
	}
}