package java8;
import java.util.Scanner;
import java.util.*;
public class Question12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the upper limit of the series");
		int n=s.nextInt(),i=0;
		int u1=1,u2=4,u3=7,temp=0;
		System.out.print(u1+","+u2+","+u3+",");
		while(i<=n) {
			temp=u1+u2+u3;
			System.out.print(temp+",");
			u1=u2;
			u2=u3;
			u3=temp;
			
			i++;
		}
	}

}