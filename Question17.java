package java8;
import java.util.Scanner;
public class Question17 {
	public static void main(String args[])
	{
	Scanner s=new Scanner(System.in);
	System.out.println("Enter the size of the array:");
	int n=s.nextInt();
	int a[]=new int[n];
	System.out.println("Enter the elements:");
	for(int i=0;i<n;i++)
	{
		a[i]=s.nextInt();
	}
	System.out.println("Elements present in the array are:");
	for(int j=0;j<n;j++)
	{
		System.out.print(a[j]+" ");
	}
	System.out.println();
	System.out.println("Enter the key element to be searched:");
	int key=s.nextInt();
	int flag=0;
	int index=0;
	for(int k=0;k<n;k++)
	{
		if(a[k]==key)
		{
			index=k;
			flag=1;
			break;
		}
	}
	if(flag==1)
	{
		System.out.println("Key is found at index:"+index);
		
	}
	else
	{
		System.out.println("Key not found!");
	}

}
}

