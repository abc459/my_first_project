package java8;
import java.util.Scanner;
public class Question20 {
	public static void main(String args[])
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the value of n:");
		int n=s.nextInt();
		int flag=0;
		int a[][]=new int[n][n];
		System.out.println("Enter the elements of the Matrix:");
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				a[i][j]=s.nextInt();
			}
		}
		
		for(int i=1;i<n;i++)
		{
			for(int j=0;j<i;j++)
			{
				if(a[i][j]==a[j][i])
				{
					continue;
				}
				else
				{
					flag=1;
					break;
				}
			}
			if(flag==1)
			{
				break;
			}
		}
		if(flag==0)
		{
			System.out.println("Symmetric Matrix!");
		}
		else
		{
			System.out.println("Not a Symmetric Matrix!");
		}
	}

}
