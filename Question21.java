package java8;
import java.util.Scanner;
public class Question21 {
public static void main(String args[])
{
	Scanner s=new Scanner(System.in);
	System.out.println("Enter the number of rows:");
	int m=s.nextInt();
	System.out.println("Enter the number of columns:");
	int n=s.nextInt();
	int a[][]=new int[m][n];
	System.out.println("Enter the elements:");
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			a[i][j]=s.nextInt();
		}
	}
	System.out.println("Elements present in the matrix are:");
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			System.out.print(a[i][j]+" ");
		}
		System.out.println();
	}
	System.out.println("Transpose of the matrix:");
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			System.out.print(a[j][i]+" ");
		}
		System.out.println();
	}
	s.close();
}
}
