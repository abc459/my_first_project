package java8;
import java.util.Scanner;

public class Question14 {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the number of rows :");
		int n=s.nextInt();
		int a=1;
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<i+1;j++)
			{
				int ans=a*a;
				if(j%2==0)
				{
					ans=ans*(-1);
					System.out.print(ans+" ");
				}
				else
				{
					System.out.print(ans+" ");
				}
				a+=1;
			}
			System.out.println();
		}
		

	}

}
