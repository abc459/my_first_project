package java8;

public class Question8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("1st,2nd,4th multiples of 7 which gives remainder 1 when divided by 2,3,4,5,6 are:");
		System.out.println("---------------------------------------------------------------------------------");
		int count=1;
		for(int i=7;;i=i+7)
		{
			if(i%2==1 && i%3==1 && i%4==1 && i%5==1 && i%6==1)
			{
				if(count==3)
				{
					count+=1;
					
				}
				else if(count==1 || count==2 || count==4)
				{
				System.out.println(i+" ");
				count+=1;
				}
				else
				{
					break;
				}
			}	
		}
	}

}
