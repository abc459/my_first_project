package java8;
import java.util.Scanner;
public class Question7 {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter an integer n:");
		int n=s.nextInt();
		int a=1;
		int b=1;
		int c=0;
		for(int i=1;i<=n;i++)
		{
			if(i==1||i==2)
			{
				System.out.print(a+" ");
			}
			else
			{
				c=a+b;
				System.out.print(c+" ");
				a=b;
				b=c;
			}
		}
	}

}
