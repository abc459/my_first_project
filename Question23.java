package java8;

import java.util.Scanner;
class sum
{
	public int sum1(int n)
	{
		int ans=0;
		while(n!=0)
		{
			ans=ans+(n%10);
			n=n/10;
		}
		return ans;
	}
}
public class Question23 {
	public static void main(String args[])
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the value of n:");
		int n=s.nextInt();
		int temp=800;
		int count=0;
		sum a=new sum();
		for(int i=1;;i++)
		{
			int temp1=temp;
			int ans1=a.sum1(temp1);
			if(ans1>=10 && ans1<=19)
			{
				count+=1;
			}
			System.out.print(temp+" ");
			temp=temp+2*i;
			if(temp>n)
			{
				break;
			}
		}
		System.out.println();
		System.out.println("The count of the sum numbers which have the digit 1 is:"+count);
	}

}
