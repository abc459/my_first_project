package java8;
import java.util.Scanner;

public class Question3 {
	public static void main(String args[])
	{
		Scanner s=new Scanner(System.in);
		System.out.println("Enter the name of the student:");
		String name=s.next();
		System.out.println("Enter the marks scored in first subject:");
		int m1=s.nextInt();
		System.out.println("Enter the marks scored in second subject:");
		int m2=s.nextInt();
		System.out.println("Enter the marks scored in third subject:");
		int m3=s.nextInt();
		int total=m1+m2+m3;
		float average=total/3;
		if(average>=60)
		{
			System.out.println("1st class");
		}
		else if(average>=50 && average<60)
		{
			System.out.println("2nd class");
		}
		else if(average>=35 && average<50)
		{
			System.out.println("pass class");
		}
		else
		{
			System.out.println("Fail");
		}
	}

}
