package java8;
import java.util.Scanner;
public class Question22 {
	public static void main(String args[])
	{
		Scanner s=new Scanner(System.in);
		int n=s.nextInt();
		for(int i=1;i<=n;i++)
		{
			for(int j=0;j<n-i;j++)
			{
				System.out.print(" ");
			}
			int temp=2*(i-1)+1;
			for(int k=0;k<temp;k++)
			{
				System.out.print("*");
			}
			System.out.println();
			
		}
		
	}

}
